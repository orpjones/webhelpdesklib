#!/usr/bin/python
from requests import post, get, put
from json import dumps
import urllib
import httplib
from abc import ABCMeta, abstractmethod
from enum import IntEnum

STS_HELPDESK_WEBHELPDESK_ACCT = 47878


class TICKET_STATUS_TYPES(IntEnum):
    OPEN = 1
    PENDING_APPROVAL = 2
    CLOSED = 3
    CANCELLED = 4
    RESOLVED = 5
    IN_PROGRESS = 6
    APPROVED = 7
    DENIED = 8
    HOLD = 9
    NOT_MY_QUEUE = 10


class ITicketAction:
    __metaclass__ = ABCMeta

    @abstractmethod
    def perform_action(self):
        return


class IActionBatch:

    @abstractmethod
    def add_action(self, ticket_action):
        return

    @abstractmethod
    def perform_all_actions(self):
        return


class ActionBatch(IActionBatch):

    def __init__(self):
        self.actions = []

    def add_action(self, ticket_action):
        self.actions.append(ticket_action)

    def perform_all_actions(self):
        for action in self.actions:
            action.perform_action()


class TicketAction(ITicketAction):

    BASE_URL = "https://helpdesk.wustl.edu/helpdesk/WebObjects/Helpdesk.woa/"
    API_KEY = 'Ay1EFuvfr6uZ5cP1Tl9Lki6FnY9Oza5QTD6VTf6S'
    API_KEY_TEXT = "?apiKey="
    TICKETS_SUB_URL = "ra/Tickets/"

    def __init__(self, ticket_number):
        self._ticket_number = ticket_number
        self._last_response = None

    def perform_action(self):
        return

    def get_ticket_number(self):
        return self._ticket_number

    @staticmethod
    def post_request(url, payload):
        return post(url, data=payload)

    @staticmethod
    def get_request(url):
        return get(url)


class CreateTicketAction(TicketAction):
    CREATE_TICKETS_URL = TicketAction.BASE_URL + TicketAction.TICKETS_SUB_URL + TicketAction.API_KEY_TEXT + TicketAction.API_KEY

    def __init__(self, ticket_subject, ticket_detail, room="", email_client=False,
                 email_tech=False, email_tech_group_level=False, email_group_manager=False,
                 email_cc=False, cc_addresses_for_tech="", email_bcc=False,
                 bcc_addresses="", assign_to_creating_tech=False, problem_type="RequestType",
                 problem_type_id=342, is_private=False, send_email=False, location_type="Location",
                 location_id=18, client_reporter_type="Client", client_reporter_id=STS_HELPDESK_WEBHELPDESK_ACCT,
                 status_type="StatusType", status_type_id=TICKET_STATUS_TYPES.OPEN,
                 priority_type="PriorityType", priority_type_id=3):
        super(CreateTicketAction, self).__init__(0)
        self.ticket_subject = ticket_subject
        self.ticket_detail = ticket_detail
        self.room = room
        self.email_client = email_client
        self.email_tech = email_tech
        self.email_tech_group_level = email_tech_group_level
        self.email_group_manager = email_group_manager
        self.email_cc = email_cc
        self.cc_addresses_for_tech = cc_addresses_for_tech
        self.email_bcc = email_bcc
        self.bcc_addresses = bcc_addresses
        self.assign_to_creating_tech = assign_to_creating_tech
        self.problem_type = problem_type
        self.problem_type_id = problem_type_id
        self.is_private = is_private
        self.send_email = send_email
        self.location_type = location_type
        self.location_id = location_id
        self.client_reporter_type = client_reporter_type
        self.client_reporter_id = client_reporter_id
        self.status_type = status_type
        self.status_type_id = status_type_id
        self.priority_type = priority_type
        self.priority_type_id = priority_type_id

    def get_json_payload(self):
        return dumps({
            "room": self.room,
            "emailClient": self.email_client,
            "emailTech": self.email_tech,
            "emailTechGroupLevel": self.email_tech_group_level,
            "emailGroupManager": self.email_group_manager,
            "emailCc": self.email_cc,
            "ccAddressesForTech": self.cc_addresses_for_tech,
            "emailBcc": self.email_bcc,
            "bccAddresses": self.bcc_addresses,
            "subject": self.ticket_subject,
            "detail": self.ticket_detail,
            "assignToCreatingTech": self.assign_to_creating_tech,
            "problemtype":
            {
                "type": self.problem_type,
                "id": self.problem_type_id,
            },
            "isPrivate": self.is_private,
            "sendEmail": self.send_email,
            "location":
            {
                "type": self.location_type,
                "id": self.location_id,
            },
            "clientReporter":
            {
                "type": self.client_reporter_type,
                "id": self.client_reporter_id,
            },
            "statustype":
            {
                "type": self.status_type,
                "id": self.status_type_id.value,
            },
            "prioritytype":
            {
                "type": self.priority_type,
                "id": self.priority_type_id,
            }
        })

    def perform_action(self):
        request_response = self.post_request(self.CREATE_TICKETS_URL, self.get_json_payload())
        self._last_response = request_response
        request_response.raise_for_status()
        self._ticket_number = request_response.json()['id']
        return request_response.status_code == httplib.CREATED


class AddNoteAction(TicketAction):
    NOTES_SUB_URL = "ra/TechNotes/"
    ADD_NOTE_URL = TicketAction.BASE_URL + NOTES_SUB_URL + TicketAction.API_KEY_TEXT + TicketAction.API_KEY

    def __init__(self, ticket_number, note_text, is_hidden=True, is_solution=False,
                 email_client=False, email_tech=False,email_cc=False, cc_addresses="",
                 email_bcc=False, bcc_addresses="",work_time="0", job_ticket_type="JobTicket",
                 email_tech_group=False, email_group_manager=False):

        super(AddNoteAction, self).__init__(ticket_number)
        self._note_id = 0

        self.note_text = note_text
        self.job_ticket_type = job_ticket_type
        self.is_hidden = is_hidden
        self.is_solution = is_solution
        self.work_time = work_time
        self.email_client = email_client
        self.email_tech = email_tech
        self.email_cc = email_cc
        self.email_bcc = email_bcc
        self.email_tech_group = email_tech_group
        self.email_group_manager = email_group_manager
        self.cc_addresses = cc_addresses
        self.bcc_addresses = bcc_addresses

    def get_json_payload(self):
        return dumps({
            "noteText": self.note_text,
            "jobticket":
            {
                "type": self.job_ticket_type,
                "id": self._ticket_number
            },
            "workTime": self.work_time,
            "isHidden": self.is_hidden,
            "isSolution": self.is_solution,
            "emailClient": self.email_client,
            "emailTech": self.email_tech,
            "emailTechGroupLevel": self.email_tech_group,
            "emailGroupManager": self.email_group_manager,
            "emailCc": self.email_cc,
            "emailBcc": self.email_bcc,
            "ccAddressesForTech": self.cc_addresses,
            "bccAddresses": self.bcc_addresses
        })

    def perform_action(self):
        request_response = self.post_request(self.ADD_NOTE_URL, self.get_json_payload())
        self._last_response = request_response
        request_response.raise_for_status()
        request_succeeded = request_response.status_code == httplib.CREATED
        if request_succeeded:
            self._note_id = self._last_response.json()['id']
        return request_succeeded

    def get_note_id(self):
        return self._note_id


class EmailNoteToClientAction(TicketAction):
    EMAIL_SUB_URL = "ra/Email/"
    EMAIL_URL = TicketAction.BASE_URL + EMAIL_SUB_URL + TicketAction.API_KEY_TEXT + TicketAction.API_KEY

    def __init__(self, ticket_number, note_id, full_details=False):
        super(EmailNoteToClientAction, self).__init__(ticket_number)
        self.note_id = note_id
        self.full_details = full_details

    def get_json_payload(self):
        return dumps({"techNoteId": str(self.note_id), "fullDetails": self.full_details})

    def perform_action(self):
        if isClientInactive(self._ticket_number):
            return False, "Cannot email client, marked as inactive."
        request_response = post(self.EMAIL_URL, data=self.get_json_payload())
        request_response.raise_for_status()
        self._last_response = request_response
        return request_response.status_code == httplib.CREATED


class UpdateStatusAction(TicketAction):

    def __init__(self, ticket_number, status_type_id):
        super(UpdateStatusAction, self).__init__(ticket_number)
        self.status_type_id = status_type_id
        self.UPDATE_TICKET_URL = TicketAction.BASE_URL + TicketAction.TICKETS_SUB_URL + unicode(self._ticket_number) +\
                                 TicketAction.API_KEY_TEXT + TicketAction.API_KEY

    def get_json_payload(self):
        return dumps({
            "statustype": {
                "type": "StatusType",
                "id": self.status_type_id.value
            }})

    def perform_action(self):
        request_response = put(self.UPDATE_TICKET_URL, data=self.get_json_payload())
        request_response.raise_for_status()
        return request_response.status_code == httplib.OK


class GetTicketDetailsAction(TicketAction):

    def __init__(self, ticket_number):
        super(GetTicketDetailsAction, self).__init__(ticket_number)
        self.TICKETS_DETAIL_URL = TicketAction.BASE_URL + TicketAction.TICKETS_SUB_URL +\
                                  unicode(self._ticket_number) + TicketAction.API_KEY_TEXT +\
                                  TicketAction.API_KEY

    def perform_action(self):
        request_response = get(self.TICKETS_DETAIL_URL)
        request_response.raise_for_status()
        self._last_response = request_response
        return request_response.status_code == httplib.OK


class GetClientDetailsAction(GetTicketDetailsAction):

    CLIENT_REPORTER_KEY = "clientReporter"
    FIRST_NAME_KEY = "firstName"
    LAST_NAME_KEY = "lastName"

    def __init__(self, ticket_number):
        super(GetClientDetailsAction, self).__init__(ticket_number)
        self._client_name = None

    def perform_action(self):
        succeeded = super(GetClientDetailsAction, self).perform_action()
        if succeeded:
            client_json = self._last_response.json()[GetClientDetailsAction.CLIENT_REPORTER_KEY]
            self._client_name = unicode(client_json[GetClientDetailsAction.FIRST_NAME_KEY] + " " +
                                        client_json[GetClientDetailsAction.LAST_NAME_KEY])
        return succeeded

    def get_client_name(self):
        return self._client_name


class GetTicketStatusAction(GetTicketDetailsAction):

    def __init__(self, ticket_number):
        super(GetTicketStatusAction, self).__init__(ticket_number)
        self.ticket_status = TICKET_STATUS_TYPES.OPEN

    def perform_action(self):
        succeeded = super(GetTicketStatusAction, self).perform_action()
        if succeeded:
            self.ticket_status = self._last_response.json()['statusTypeId']
        return succeeded

    def get_status(self):
        return self.ticket_status

############################## LEGACY #################################################


API_KEY = 'Ay1EFuvfr6uZ5cP1Tl9Lki6FnY9Oza5QTD6VTf6S'
API_KEY_TEXT = "?apiKey="

#URLS
BASE_URL = "https://helpdesk.wustl.edu/helpdesk/WebObjects/Helpdesk.woa/"
TICKETS_URL = "ra/Tickets/"
NOTES_URL = "ra/TechNotes/"
EMAIL_URL = "ra/Email/"
CLIENTS_URL = "ra/Clients/"
CLIENT_SEARCH_URL = "&qualifier="

class URL_TYPES(IntEnum):
    GET_TICKET = 1
    NOTES = 2
    EMAIL = 3
    CREATE_TICKET = 4
    CLIENT = 5
    CLIENT_SEARCH = 6


def getURLForClientSearch(searchTerms):
    return getURL(URL_TYPES.CLIENT_SEARCH, 0, searchTerms)


def getURLForTicket(ticketNumber):
    return getURL(URL_TYPES.GET_TICKET, ticketNumber, "")


def getURL(url_type, ticket_number=0, search_terms=""):
    # If only they had switch statements in python...
    if url_type == URL_TYPES.GET_TICKET:
        return BASE_URL + TICKETS_URL + str(ticket_number) + API_KEY_TEXT + API_KEY
    elif url_type == URL_TYPES.NOTES:
        return BASE_URL + NOTES_URL + API_KEY_TEXT + API_KEY
    elif url_type == URL_TYPES.EMAIL:
        return BASE_URL + EMAIL_URL + API_KEY_TEXT + API_KEY
    elif url_type == URL_TYPES.CREATE_TICKET:
        return BASE_URL + TICKETS_URL + API_KEY_TEXT + API_KEY
    elif url_type == URL_TYPES.CLIENT:
        return BASE_URL + CLIENTS_URL
    elif url_type == URL_TYPES.CLIENT_SEARCH:
        # ignore characters '@*
        search_terms = urllib.quote(search_terms, "'@*")
        return BASE_URL + CLIENTS_URL + API_KEY_TEXT + API_KEY + CLIENT_SEARCH_URL + "(" + search_terms + ")"
    else:
        return ""


def verifyClientName(directory_client_name, ticket_number):
    client_json = getClientJSON(ticket_number)
    first_name = str(client_json.get('firstName'))
    last_name = str(client_json.get('lastName'))
    client_name = first_name + " " + last_name

    directory_client_name = directory_client_name.strip()
    client_name = client_name.strip()
    if directory_client_name in client_name:
        return True
    else:
        return False


def getClientJSON(ticket_number):
    return getTicketJSON(ticket_number)[1]['clientReporter']


def getTicketJSON(ticket_number):
    url = getURL(URL_TYPES.GET_TICKET, ticket_number)
    r = get(url)
    return r.status_code == httplib.OK, r.json()

def createTicket(subject, detail, room="", email_client=False, email_tech=False, email_tech_group_level=False,
                 email_group_manager=False, email_cc=False, cc_addresses_for_tech="", email_bcc=False,
                 bcc_addresses="", assign_to_creating_tech=False, problem_type="RequestType", problem_type_id=342,
                 is_private=False, send_email=False, location_type="Location", location_id=18, client_reporter_type="Client",
                 client_reporter_id=STS_HELPDESK_WEBHELPDESK_ACCT, status_type="StatusType",
                 status_type_id=TICKET_STATUS_TYPES.OPEN, priority_type="PriorityType", priority_type_id=3):
    payload = dumps(
        {
            "room": room,
            "emailClient": email_client,
            "emailTech": email_tech,
            "emailTechGroupLevel": email_tech_group_level,
            "emailGroupManager": email_group_manager,
            "emailCc": email_cc,
            "ccAddressesFo  rTech": cc_addresses_for_tech,
            "emailBcc": email_bcc,
            "bccAddresses": bcc_addresses,
            "subject": subject,
            "detail": detail,
            "assignToCreatingTech": assign_to_creating_tech,
            "problemtype":
            {
                "type": problem_type,
                "id": problem_type_id,
            },
            "isPrivate": is_private,
            "sendEmail": send_email,
            "location":
            {
                "type": location_type,
                "id": location_id,
            },
            "clientReporter":
            {
                "type": client_reporter_type,
                "id": client_reporter_id,
            },
            "statustype":
            {
                "type": status_type,
                "id": status_type_id.value,
            },
            "prioritytype":
            {
                "type": priority_type,
                "id": priority_type_id,
            }
        })
    url = getURL(URL_TYPES.CREATE_TICKET)
    r = post(url, data=payload)
    return r.status_code == httplib.CREATED, r

def addNoteAndEmailToClient(ticket_number, text, full_details=False):
    value = addNoteToTicket(ticket_number, text, is_hidden=False, email_client=True)
    if value[0]:
        note_id = value[1].json()['id']
        return emailNoteToClient(ticket_number, note_id)


def emailNoteToClient(ticket_number, note_id, full_details=False):
    if isClientInactive(ticket_number):
        return False, "Cannot email client, marked as inactive."
    else:
        payload = dumps({"techNoteId": str(note_id), "fullDetails": full_details})
        url = getURL(URL_TYPES.EMAIL)
        r = post(url, data=payload)
        return r.status_code == httplib.CREATED, r

def isClientInactive(ticketNumber):
    values = getTicketJSON(ticketNumber)
    if values[0]: # If succeeded
        json = values[1] # Get the JSON values

        # If client display has a [D] in it, the client is inactive and WHD won't email them.
        client_display_name = json['displayClient']
        return "[D]" in client_display_name

def addNoteToTicket(ticket_number, note_text, ticket_status=None, is_hidden=True,
                    is_solution=False, email_client=False, email_tech=False,
                    email_cc=False, cc_addresses="", bcc_addresses="", email_bcc=False,
                    work_time="0", job_ticket_type="JobTicket", email_tech_group=False,
                    email_group_manager=False):

    # Don't change ticket status when adding, unless it is closed. In which case put it on hold.
    if ticket_status is None:
        response_json = getTicketJSON(ticket_number)
        if response_json[0]:
            ticket_status = response_json[1]['statusTypeId']
            if ticket_status == TICKET_STATUS_TYPES.CLOSED:
                ticket_status = TICKET_STATUS_TYPES.HOLD.value
    else:
        ticket_status = ticket_status.value

    payload = dumps({
              "noteText": note_text,
              "statusTypeId": ticket_status,
              "isHidden": is_hidden,
              "isSolution": is_solution,
              "emailClient": email_client,
              "emailTech": email_tech,
              "emailCc": email_cc,
              "ccAddressesForTech": cc_addresses,
              "emailBcc": email_bcc,
              "bccAddresses": bcc_addresses,
              "workTime": work_time,
              "ticketId": ticket_number,
              "id": "0",
              "jobticket":
              {
                  "type": job_ticket_type,
                  "id": ticket_number
              },
              "emailTechGroupLevel": email_tech_group,
              "emailGroupManager": email_group_manager
    })
    url = getURL(URL_TYPES.NOTES)
    r = post(url, data=payload)
    return r.status_code == httplib.CREATED, r

def mapEmailAddressToWUSTLKey(email_address):
    url = getURLForClientSearch("email='" + email_address + "'")
    r = get(url)
    username = ""
    if r.status_code == httplib.OK:
        json = r.json()
        if json:  # if valid
            username = str(json[0]['username'])
    return (r.status_code == httplib.OK and len(username) > 0), username