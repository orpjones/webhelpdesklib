import unittest
from WebHelpDeskAPILib import ITicketAction, ActionBatch, \
    CreateTicketAction, AddNoteAction, UpdateStatusAction,\
    EmailNoteToClientAction, GetTicketDetailsAction, \
    GetTicketStatusAction, GetClientDetailsAction, \
    TICKET_STATUS_TYPES, getClientJSON, getTicketJSON,\
    addNoteToTicket, addNoteAndEmailToClient, createTicket


class TicketActionTests(unittest.TestCase):

    TEST_TICKET_NUMBER = 176418

    def test_create(self):
        create_ticket_action = CreateTicketAction("Test ticket", "Testing ticket creation.")
        self.assertTrue(create_ticket_action.perform_action())
        print "Created ticket " + str(create_ticket_action.get_ticket_number())
        # Close anything we create
        self.assertTrue(UpdateStatusAction(create_ticket_action.get_ticket_number(), TICKET_STATUS_TYPES.CLOSED)
                        .perform_action())

    def test_add_note(self):
        add_note_action = AddNoteAction(self.TEST_TICKET_NUMBER, "Testing add note")
        self.assertTrue(add_note_action.perform_action())

    def test_update_status(self):
        update_status_action = UpdateStatusAction(self.TEST_TICKET_NUMBER, TICKET_STATUS_TYPES.CLOSED)
        self.assertTrue(update_status_action.perform_action())

    def test_get_ticket_details(self):
        get_ticket_details_action = GetTicketDetailsAction(self.TEST_TICKET_NUMBER)
        self.assertTrue(get_ticket_details_action.perform_action())

    def test_get_ticket_status(self):
        get_ticket_status_action = GetTicketStatusAction(self.TEST_TICKET_NUMBER)
        self.assertTrue(get_ticket_status_action.perform_action())
        self.assertEquals(get_ticket_status_action.get_status(), TICKET_STATUS_TYPES.CLOSED)

    def test_get_client_details(self):
        get_client_details_action = GetClientDetailsAction(self.TEST_TICKET_NUMBER)
        self.assertTrue(get_client_details_action.perform_action())
        self.assertEquals(get_client_details_action.get_client_name(), "STS Helpdesk")

    def test_email_note_action(self):
        add_note_to_ticket_action = AddNoteAction(self.TEST_TICKET_NUMBER, "Testing email note", is_hidden=False)
        self.assertTrue(add_note_to_ticket_action.perform_action())
        print add_note_to_ticket_action.get_note_id()

        email_note_to_client_action = EmailNoteToClientAction(self.TEST_TICKET_NUMBER,
                                                              add_note_to_ticket_action.get_note_id())
        self.assertTrue(email_note_to_client_action.perform_action())


class DummyAction(ITicketAction):

    def __init__(self):
        return

    def perform_action(self):
        return True


class BatchTicketActionsTests(unittest.TestCase):

    def test_add(self):
        batch = ActionBatch()
        self.assertEquals(len(batch.actions), 0)
        batch.add_action(DummyAction())
        self.assertEquals(len(batch.actions), 1)
        batch.add_action(DummyAction())
        self.assertEquals(len(batch.actions), 2)

    def test_perform_all(self):
        batch = ActionBatch()
        batch.add_action(DummyAction())
        batch.add_action(DummyAction())
        batch.perform_all_actions()


class LegacyTests(unittest.TestCase):

    # WHD ticket created to test things
    TEST_TICKET_NUMBER = 176419

    def test_create_ticket(self):
        self.assertTrue(createTicket("Legacy ticket creation test", "Legacy ticket creation detail")[0])

    def test_get_ticket_json(self):
        self.assertTrue(getTicketJSON(LegacyTests.TEST_TICKET_NUMBER)[0])

    def test_get_client_json(self):
        result = getClientJSON(LegacyTests.TEST_TICKET_NUMBER)
        self.assertTrue(result[0])
        self.assertEquals(result['firstName'], unicode("STS"))
        self.assertEquals(result['lastName'], unicode("Helpdesk"))

    def test_add_note_to_ticket(self):
        self.assertTrue(addNoteToTicket(LegacyTests.TEST_TICKET_NUMBER, "Test ticket note - legacy")[0])

    def test_add_note_to_closed_ticket(self):
        # Test on closed ticket
        self.assertTrue(addNoteToTicket(LegacyTests.TEST_TICKET_NUMBER, "Test ticket note - legacy")[0])

    def test_add_note_and_email_to_client(self):
        self.assertTrue(addNoteAndEmailToClient(LegacyTests.TEST_TICKET_NUMBER, "Test email ticket note - legacy")[0])


if __name__ == '__main__':
    unittest.main()